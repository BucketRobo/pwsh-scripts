#Requires -Version 3 -Modules ActiveDirectory
Function Start-ADReport {
    <#
    .Synopsis
    Function to generate AD reports in CSV format
    
    .Description
    Function to generate AD reports for Domain Controllers, Servers, Workstations, and Users.
    
    .Parameter All
    Run and export all reports (Domain Controllers, Servers, Workstations, and Users)
    This is the default action
    
    .Parameter Users
    Run and export the Users report

    .Parameter Groups
    Run and export the Groups report
    
    .Parameter Servers
    Run and export the Servers report
    
    .Parameter Workstations
    Run and export the Workstations report

    .Parameter DomainControllers
    Run and export a report on Domain Controllers

    .Example
    Start-ADReport -All
    
    This example shows how to run all reports and exports CSV files to C:\temp\Reportname.csv (DomainControllers.csv, etc.)

    .Example
    Start-ADReport -OutPath C:\Reports\ActiveDirectory\

    This example shows how to run all reports, and export them into the C:\Reports\ActiveDirectory\ directory
    The path must include a trailing backslash "\"
    
    .Example
    Start-ADReport -Servers -OutPath D:\Reports\
    
    This example shows how to run a report on Servers and export the CSV to an alternate path (D:\Reports\DOMAIN-LOCAL-``Servers.csv)
    The function will automatically name the report based on the report type chosen, include ONLY the path and not the filename or extension

    .Notes
        Function by Luke Zedler
        V0.5 - 12/9/2019 - Script created
        V1.0 - 12/11/2019 - Converted to function and fixed functionality
        V1.2 - 12/12/2019 - Cleaned up and streamlined code
        V1.3.1 - 12/12/2019 - Created additional example for Default option since default wasn't working before
        V1.4 - 12/20/2019 - Replace If/Else inside Process block with Switch statement; Need refinement (may not work now)
        V1.4.5 - 01/10/2020 - Replace non-working hashtable in switch statement with a proper OrderedDictionary to support Insert method; Remove "$MemberOf" variable (not used)
        V1.4.9 - 01/14/2020 - Working on $Report output for multiple conditions called by All or Default - Function still not fully functional (hah!)
        V1.4.9.1 - 01/16/2020 - $Report variable mostly working. Logic to iterate through parametersetname in ForEach loop needed. Currently it doesn't work properly.
        V1.4.9.2 - 02/14/2020 - Remove $OutPath variable, start to replace with logic for multiple outputs
        v1.5.0 - 03/11/2020 - Fix switches for reports, Fix $Report variable so it works properly; Add paths for reports to $Report variable during report creation; Remove $computers variable and replace with individual queries to cut down on report generation time for single reports; Re-introduce $OutPath variable-Renamed from $Path due to possible confusion
        v1.5.1 - 03/12/2020 - Fix $Report switch to properly iterate through the variable and then produce CSV files per report
        v1.5.2 - 03/17/2020 - Add Groups parameter and variables for reporting
        v1.5.3 - 03/23/2020 - Fix ServicePrincipalName and OperationMasterRoles not expanding for outputs

        Next Version:
        Add logic to not require trailing slash in $OutPath variable
        Set up parameters to allow for multiple parameters (E.G. Users and Groups, or Servers and Users, etc.)?
        Add parameter for all computers report instead of breakout of workstations, servers, DCs??
        Add DNS lookup for NameServers, Static records, and CNAMEs
    #>


    [CmdletBinding(DefaultParameterSetName = "All")]

    param (
        [Parameter(Mandatory = $false,
            Position = 0,
            ParameterSetName = "All")]
        [switch]$All,
        
        [Parameter(Mandatory = $true,
            ParameterSetName = "Servers")]
        [switch]$Servers,
        
        [Parameter(Mandatory = $true,
            ParameterSetName = "Workstations")]
        [switch]$Workstations,
        
        [Parameter(Mandatory = $true,
            ParameterSetName = "DomainControllers")]
        [switch]$DomainControllers,
        
        [Parameter(Mandatory = $true,
            ParameterSetName = "Groups")]
        [switch]$Groups,
        
        [Parameter(Mandatory = $true,
            ParameterSetName = "Users")]
        [switch]$Users,
        
        [Parameter(Mandatory = $false)]
        [string]$OutPath = 'C:\temp\'
    )
        
    begin {
        $ErrorActionPreference = "Stop"
        Write-Output 'Creating variables for input...'
        $Domain = Get-ADDomain
        Try {
            Switch -Regex ($PSCmdlet.ParameterSetName) {
                "All|Default|Users" { $ADUsers = Get-ADUser -Filter * -Properties Name, DistinguishedName, UserPrincipalName, Enabled, EmailAddress, HomeDirectory, HomeDrive, ScriptPath, AccountExpirationDate, LastLogonDate, PasswordExpired, PasswordNeverExpires, LockedOut, MemberOf, ProfilePath, ManagedBy }
                "All|Default|Groups" { $ADGroups = Get-ADGroup -Filter * -Properties Name, ManagedBy, GroupCategory, GroupScope, GroupType, mail, mailNickname, showInAddressBook, MemberOf, Members, DistinguishedName, ObjectGUID, SID }
                "All|Default|Servers" { $ADServers = Get-ADComputer -LDAPFilter "(&(objectCategory=computer)(operatingSystem=*server*)(!(userAccountControl:1.2.840.113556.1.4.803:=8192)))" -Properties OperatingSystem, IPv4Address, MemberOf, LastLogonDate, Created, ManagedBy, LockedOut, ObjectGUID, SID, ServicePrincipalName }
                "All|Default|Workstations" { $ADWorkstations = Get-ADComputer -LDAPFilter "(&(objectCategory=computer)(!(operatingSystem=*server*)))" -Properties OperatingSystem, IPv4Address }
                "All|Default|DomainControllers" { $ADDCs = Get-ADDomainController -Filter * }
            } # Switch
        } # Try
        Catch {
            Write-Host "An Error Occurred creating the report variables: $Error[0..1]"
        } # Catch
    } # Begin
        
    process {
        $Report = [System.Collections.Specialized.OrderedDictionary]@{ }
        Switch -Regex ($PSCmdlet.ParameterSetName) {
            "All|Default|Users" { 
                $Report.Add('UserReport', ($ADUsers | Select-Object Name, GivenName, Surname, EmailAddress, HomeDirectory, HomeDrive, ScriptPath, ProfilePath, ManagedBy, Enabled, LastLogonDate, PasswordExpired, PasswordNeverExpires, LockedOut, AccountExpirationDate, @{Name = "MemberOf"; Expression = { $_.MemberOf -replace '(CN=)(.*?),.*', '$2' -join ', ' } }, SID, ObjectGUID | Sort-Object -Property Name)) 
                $Report.Add('UserPath', ($OutPath + ($Domain.Forest -replace ("\.", "-")).ToUpper() + "-UserReport" + ".csv"))
            }
            "All|Default|Groups" { 
                $Report.Add('GroupReport', ($ADGroups | Select-Object Name, ManagedBy, @{Name = 'Nested'; Expression = { [bool]$_.MemberOf } }, @{Name = 'Members'; Expression = { $_.Members -replace '(CN=)(.*?),.*', '$2' -join ', ' } }, @{Name = 'MemberOf'; Expression = { $_.MemberOf -replace '(CN=)(.*?),.*', '$2' -join ', ' } }, ObjectGUID, SID | Sort-Object -Property Name)) 
                $Report.Add('GroupPath', ($OutPath + ($Domain.Forest -replace ("\.", "-")).ToUpper() + "-GroupReport" + ".csv"))
            }
            "All|Default|Servers" { 
                $Report.Add('ServerReport', ($ADServers | Select-Object Name, OperatingSystem, IPv4Address, Enabled, LastLogonDate, LockedOut, Created, ManagedBy, @{Name = 'MemberOf'; Expression = { $_.MemberOf -replace '(CN=)(.*?),.*', '$2' -join ', ' } }, ObjectGUID, SID, @{Name = 'ServicePrincipalName'; Expression = { $_ | Select-Object -ExpandProperty ServicePrincipalName } } | Sort-Object -Property Name) )
                $Report.Add('ServerPath', ($OutPath + ($Domain.Forest -replace ("\.", "-")).ToUpper() + "-ServerReport" + ".csv" )) 
            }
            "All|Default|Workstations" { 
                $Report.Add('WorkstationReport', ($ADWorkstations | Select-Object Name, OperatingSystem, IPv4Address, Enabled, LastLogonDate, LockedOut, Created, ManagedBy, @{Name = 'MemberOf'; Expression = { $_.MemberOf -replace '(CN=)(.*?),.*', '$2' -join ', ' } }, ObjectGUID, SID, @{Name = 'ServicePrincipalName'; Expression = { $_ | Select-Object -ExpandProperty ServicePrincipalName } } | Sort-Object -Property Name)) 
                $Report.Add('WorkstationPath', ($OutPath + ($Domain.Forest -replace ("\.", "-")).ToUpper() + "-WorkstationReport" + ".csv"))
            }
            "All|Default|DomainControllers" { 
                $Report.Add('DomainControllerReport', ($ADDCs | Select-Object Name, OperatingSystem, IPv4Address, Domain, IsGlobalCatalog, IsReadOnly, LDAPPort, @{Name = 'OperationMasterRoles'; Expression = { ($_ | Select-Object -ExpandProperty OperationMasterRoles) -replace ' ' -join ', ' } }, ServerObjectGUID, Site | Sort-Object -Property Name))
                $Report.Add('DomainControllerPath', ($OutPath + ($Domain.Forest -replace ("\.", "-")).ToUpper() + "-DomainControllerReport" + ".csv"))
            }
        } # Switch
    } # Process
    
    end {
        Write-Output "Exporting results to CSV file(s)..."
        Switch ($Report) {
            { [bool]$_.UserReport } { $Report.UserReport | Export-CSV -Path $Report.UserPath -NoTypeInformation -Force }
            { [bool]$_.GroupReport } { $Report.GroupReport | Export-CSV -Path $Report.GroupPath -NoTypeInformation -Force }
            { [bool]$_.ServerReport } { $Report.ServerReport | Export-CSV -Path $Report.ServerPath -NoTypeInformation -Force }
            { [bool]$_.WorkstationReport } { $Report.WorkstationReport | Export-CSV -Path $Report.WorkstationPath -NoTypeInformation -Force }
            { [bool]$_.DomainControllerReport } { $Report.DomainControllerReport | Export-CSV -Path $Report.DomainControllerPath -NoTypeInformation -Force }
        } # Switch
        Write-Output "Report(s) created. They are located in the $($OutPath) directory. `nThanks, and have a good day!"
    } # End

} # Function